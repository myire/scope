# Scope - MyIRE Generic Data Visualization Library for web2py

# Installation Instructions

## Mac OSX 10.10.1 (14B25)

1. Create a new virtualenv:

        cd ~
        mkdir -p scope
        cd scope
        virtualenv .

2. Enter into it:

        source bin/activate

3. Install matplotlib just in your virtualenv:

        pip install matplotlib

4. Download latest web2py stable:

        wget http://www.web2py.com/examples/static/web2py_src.zip

5. Unzip it:

        unzip web2py_src.zip

6. Enter the web2py directory:

        cd web2py

7. Get the latest application code:

        git clone https://bitbucket.org/myire/scope.git applications/scope

# Supported Visualization Libraries (draft)

## Python

1. [matplotlib](http://matplotlib.org/)
2. [ggplot](http://ggplot.yhathq.com/)
3. [seaborn](http://stanford.edu/~mwaskom/software/seaborn/)
4. [python-igraph](http://igraph.org/python/)

## javascript

1. [d3.js][http://d3js.org/)
2. [processing.js](http://processingjs.org/)
3. [dygraphs](http://dygraphs.com/)
4. [vega](https://trifacta.github.io/vega/)
5. [vis.js](http://visjs.org/)

## java

1. [gephi](http://gephi.github.io/)

# Testing Data Sets:
1. [https://open.fda.gov/]



